const app = require("express")();
const port = process.env.PORT || 8080;

app.get("/:name", (req, res) => res.send(`Hello ${req.params.name}`));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
